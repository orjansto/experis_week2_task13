﻿using System;

namespace Task13_RandomList
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("----Task13---\n");

            Console.WriteLine("Testing <string> in caosArray");
            Console.WriteLine("This is the empty caosArray:\n");

            CaosArray<string> caosString = new CaosArray<string>(5);
            caosString.caosPrint();

            caosString.caosAdd("Orjan");
            caosString.caosAdd("Markus");
            caosString.caosAdd("Huy");
            Console.WriteLine("caosArray with <string>:\n");
            caosString.caosPrint();

            Console.WriteLine("\nEnter to continue.");
            Console.ReadLine();

            CaosArray<int> caosTest = new CaosArray<int>(5);
            Console.WriteLine("Testing <int> in caosArray");
            Console.WriteLine("This is the empty caosArray:\n");
            caosTest.caosPrint();
            
            Console.WriteLine();


            for (int i = 1; i < 7; i++)
            {
                caosTest.caosAdd(i);
                Console.WriteLine();
                caosTest.caosPrint();
            }
            

            Console.ReadLine();
            Console.WriteLine($"Adding another element {12}");
            caosTest.caosAdd(12);
            Console.WriteLine();
            caosTest.caosPrint();

            Console.WriteLine("Trying caosRemove");
             
            Console.WriteLine($"Removed: {caosTest.caosGetandRemove().ToString()}");
            Console.WriteLine($"Removed: {caosTest.caosGetandRemove().ToString()}");
            Console.WriteLine($"Removed: {caosTest.caosGetandRemove().ToString()}");
            caosTest.caosPrint();
        }
    }
}
