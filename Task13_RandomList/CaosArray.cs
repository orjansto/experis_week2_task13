﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task13_RandomList
{
    public class CaosArray<T>
    {
        private T[] caosArry;
        private Random randomSpot = new Random();
        
        public CaosArray()
        {
            caosArry = new T[10];
        }
        public CaosArray(int size)
        {
            caosArry = new T[size];
        }

        /// <summary>
        /// Adding an item to a random spot in the caosArray
        /// </summary>
        /// <param name="item"></param>
        public void caosAdd(T item)
        {
            bool retry = false;
            do
            {
                retry=false;
                int rnd;
                T yolo;
                rnd = randomSpot.Next(caosArry.Length);
                try
                {
                    yolo = caosArry[rnd];
                    if(yolo == null)
                    {
                        Console.WriteLine($"Writing into emptry space: Element {rnd + 1} is now {item}");
                        caosArry[rnd] = item;
                    }
                    else if (yolo.Equals(default(T)))
                    {
                        Console.WriteLine($"Writing into emptry space: Element {rnd + 1} is now {item}");
                        caosArry[rnd] = item;
                    }
                    else
                    {
                        throw new System.Exception($"{item.ToString()} was not added, as spot {rnd + 1} already is populated");
                    }
                    retry = false;

                }
                catch (System.Exception e)
                {
                    Console.WriteLine(e.Message);
                    foreach (T errorTest in caosArry)
                    {
                        if (errorTest == null)
                        {
                            retry = true;
                        }
                        else if (errorTest.Equals(default(T)))
                        {
                            retry = true;
                        }
                    }
                    if (!retry)
                    {
                        Console.WriteLine("No more valid places in the caosArray.");
                    }
                    else
                    {
                        Console.WriteLine("Press enter to try another spot.");
                        Console.ReadLine();
                    }
                }
            } while (retry);
        }

        /// <summary>
        /// Returns random item from the caosArray
        /// Returns null if access empty spot
        /// </summary>
        /// <returns></returns>
        public T caosGet()
        {
            int rnd;
            rnd = randomSpot.Next(caosArry.Length + 1);
            T item = caosArry[rnd];
            try
            {
                if (item == null)
                {
                    throw new Exception($"ERROR: Tries to get an item from an empty spot {rnd}");
                }
                else if (item.Equals(default(T)))
                {
                    throw new Exception($"ERROR: Tries to get an item from an empty spot {rnd}");
                }
                return item;
            }
            catch (Exception exp)
            {
                Console.WriteLine(exp.Message);
                return item;
            }
            
        }

        /// <summary>
        /// Returns random item from the caosArray
        /// Returns null if access empty spot
        /// </summary>
        /// <returns></returns>
        public T caosGetandRemove()
        {
            int rnd;
            rnd = randomSpot.Next(caosArry.Length + 1);
            T item = caosArry[rnd];
            try
            {
                if (item == null)
                {
                    throw new Exception($"ERROR: Tries to get an item from an empty spot {rnd}, nothing to remove");
                }
                else if (item.Equals(default(T)))
                {
                    throw new Exception($"ERROR: Tries to get an item from an empty spot {rnd}, nothing to remove");
                }
                caosArry[rnd] = default(T);
                return item;
            }
            catch (Exception exp)
            {
                Console.WriteLine(exp.Message);
                return item;
            }
        }

        /// <summary>
        /// Prints the caosArray to console
        /// </summary>
        public void caosPrint()
        {
            foreach (T item in caosArry)
            {
                if(item == null|| item.Equals(default(T)))
                {
                    Console.WriteLine("NULL");
                }
                else
                {
                    Console.WriteLine(item.ToString());
                }
                
            }
        }

    }
}
